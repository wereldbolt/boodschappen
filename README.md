# Boodschappen

Voor mijn zwager Ariën.

Om het verzamelen van boodschappen in de supermarkt te vergemakkelijken. 

Het idee is een Google Drive spreadsheet waarin na het selecteren van producten een boodschappenlijst wordt gegenereerd.   
Met daarin de producten gesorteerd. 

![](assets/afbeeldingen/boodschappenapp.gif)

Hieronder volgt een stappenplan om dit te maken in Google Drive.

# Google Drive Spreadsheet  
Maak in Google Drive een spreadsheet. De naam van het spreadsheet is 
willekeurig.

# Tabbladen
Noem het eerste tabblad 'boodschappenlijst'. Deze naam wordt gebruikt in 
de code en mag dus niet afwijken. Voeg een tabblad toe met de naam 'producten', zoals hieronder afgebeeld:

![](assets/afbeeldingen/tabbladen.PNG);
 
# Tabblad producten
Het tabblad producten bestaat uit 5 kolommen: pad, sortering, aantal, product, selecteren.

![](assets/afbeeldingen/tabblad-producten.PNG);

## kolom 'pad'  
Een supermarkt bestaat meestal uit paden. Voor jezelf kun je die nummeren. De supermarkt
waar ik kom heeft één pad aan de kopse kant, dat is bij mij pad 0. 
Het eerste pad bij binnenkomst is pad 1.

## kolom 'sortering'
De sortering in een pad is een schatting waar het product zich bevindt. De sortering
is vrij uit te drukken in een getal. Ik houd een sortering aan waarbij ik makkelijk
een aanpassing kan doen of een product kan toevoegen door de locatie als een percentage uit
te drukken. Dus 'ham op brood' staat ergens aan het begin van het pad. Een product
met sortering 50 staat dus halverwege het pad.

## kolom 'aantal'  
Dit is het aantal dat wordt toegevoegd aan de boodschappenlijst.

## kolom 'product'  
De naam van het product  

## kolom 'selecteren'  
Door een 'x' te plaatsen wordt het product op de boodschappenlijst geplaatst zodra
de boodschappenlijst gegenereerd wordt.

# Tabblad boodschappenlijst  
Het tabblad boodschappenlijst heeft vier kolommen: aantal, product, pad, sortering.
Dit kolomkoppen moeten blijven staan. Zodra een boodschappenlijst is gebruikt en de boodschapjes
in de kast liggen mag de rest van het tabblad boodschappenlijst leeg worden gemaakt.

# Script  
In dit project bevindt zich een bestand met het script: `src/script.js`. Het script voegt het menu 'boodschappenlijst' toe. 
Het script treedt ook in werking als op een menuitem geklikt wordt.

Open in de Google spreadsheet de Script editor door in het menu Extra op Scripteditor te klikken. Een nieuw venster opent zich. Het bestand `Code.gs`
is geopend. Plak hierin de code van `src/script.js`.

Sluit de vensters van de Google spreadsheet van de Scripteditor. Bij het openen van het spreadsheet wordt het script ingeladen
en wordt het menuitem boodschappenlijst beschikbaar. Tijdens het openen wordt gevraagd om toestemming voor toegang van het script
tot het spreadsheet. Dat is in dit geval geen probleem.   
