/*
* use cases:
* - plaats pad en sortering bij boodschappen lijst
* - markeer product in boodschappenlijst om aan te geven dat productie niet bestaat in de productenlijst
* - selecteer gerecht en  doe voorstel voor aantallen
*
* v tabblad gerechten, vink welk gerecht moet worden opgenomen
* v menu item, voeg gerechten toe
* v per gerecht de aantallen presenteren, en zo, dat de gebruiker de aantallen kan wijzigen
* formulier gegeven uitlezen
* product aantallen opnemen in boodschappenlijst, bij bestaand aantal de gegevens optellen
*
*
*
*/

/*
* -----------------------------------------------------------------------------------------
*
* UTILS
*
* -----------------------------------------------------------------------------------------
*/

/*
* Kop rij moet aanwezig zijn.
* Eerste kolom is A
* nColumns is het aantal kolommen inclusief de eerste kolom
*/
function getTableRange(sheet, nColumns){
    var lastRow = sheet.getLastRow();
    if(lastRow == 1){
        return;
    }
    return sheet.getRange(2,1, lastRow - 1, nColumns);
}

function getProductBijNaam(productValues, name){
    let arr = productValues.find(p => p[3] === name);

    if(arr){
        return {
            pad: arr[0],
            sortering: arr[1],
            aantal: arr[2],
            naam: arr[3]
        }
    }
}

function productRangeValueToObject(productRangeValue){
    var arr = productRangeValue;
    return {
        pad: arr[0],
        sortering: arr[1],
        aantal: arr[2],
        naam: arr[3]
    }
}

/*
* -----------------------------------------------------------------------------------------
*
* PRODUCTEN OPNEMEN IN DE BOODSCHAPPENLIJST
*
* -----------------------------------------------------------------------------------------
*/

/*
*
* In het tabblad producten na een dubbelklik op een product deze verplaatsen naar het einde
* van de lijst in het tabblad boodschappenlijst.
*
*/
function productenInBoodschappenlijstOpnemen() {
    var geselecteerdeProducten = lijstMetGeselecteerdeProducten();
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var shBoodschappenlijst = ss.getSheetByName('boodschappenlijst');
    //shBoodschappenlijst.getRange('A:D').activate();
    //var boodschappenlijstRange = shBoodschappenlijst.getRange(lastNumRow, 4);
    var lastRow = shBoodschappenlijst.getRange('B:B').getValues().filter(String).length;
    for(let v of geselecteerdeProducten){
        lastRow++;
        var productObject = productRangeValueToObject(v);
        shBoodschappenlijst.getRange(lastRow,1).setValue(productObject.aantal);
        shBoodschappenlijst.getRange(lastRow,2).setValue(productObject.naam);
        shBoodschappenlijst.getRange(lastRow,3).setValue(productObject.pad);
        shBoodschappenlijst.getRange(lastRow,4).setValue(productObject.sortering);
    }
};

/*
*
* Retourneren van geselecteerde producten.
* Geselecteerde producten hebben in sheet 'producten' in de kolom 'selecteren' een willekeurige waarde.
*
*/
function lijstMetGeselecteerdeProducten(){
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var shProducten = ss.getSheetByName('producten');
    var productRange = getTableRange(shProducten, 5);
    return productRange.getValues().reduce((p, c, i, a) => {

        if(c[4]){ //waarde in kolom 'selecteren' aanwezig
            p.push(c);
        }

        return p;

    }, []);
}


/*
* -----------------------------------------------------------------------------------------
*
* PAD EN SORTERING IN BOODSCHAPPENLIJST OPNEMEN
*
* -----------------------------------------------------------------------------------------
*/

/*
* Plaatst pad en sortering bij product in boodschappenlijst.
* De functie maakt gebruik van de gegevens in sheet 'producten'.
*/
function plaatsPadBijProductInBoodschappenlijst() {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var shProducten = ss.getSheetByName('producten');
    var productRange = getTableRange(shProducten, 4);

    var shBoodschappenlijst = ss.getSheetByName('boodschappenlijst');
    var boodschappenlijstRange = getTableRange(shBoodschappenlijst, 4);
    var boodschappenlijstRangeEntries = boodschappenlijstRange.getValues().entries();
    var productRangeValues = productRange.getValues();

    for(let [i,v] of boodschappenlijstRangeEntries){

        // zoek product
        var productValueObject = getProductBijNaam(productRangeValues, v[1]);

        if(productValueObject){
            // plaats product pad en sortering
            shBoodschappenlijst.getRange(i+2,3).setValue(productValueObject.pad);
            shBoodschappenlijst.getRange(i+2,4).setValue(productValueObject.sortering);

        }
    };

    boodschappenSorterenOpPadEnSortering();
}

/*
*
* Sorteren van de boodschappen op pad en sortering in het pad
*
*/
function boodschappenSorterenOpPadEnSortering() {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var spreadsheet = ss.getSheetByName('boodschappenlijst');
    spreadsheet.getRange('A:D').activate();
    spreadsheet.getActiveRange().offset(1, 0, spreadsheet.getActiveRange().getNumRows() - 1).sort([{column: 3, ascending: true}, {column: 4, ascending: true}]);
};

/*
* -----------------------------------------------------------------------------------------
*
* GERECHTEN VERWERKEN IN BOODSCHAPPENLIJST
*
* -----------------------------------------------------------------------------------------
*/

/*
* Menuitem producten van gerechten opnemen in boodschappenlijst na selectie in modal.
*/
function onOpen() {
    var spreadsheet = SpreadsheetApp.getActive();
    var menuItems = [
        {name: 'Gerechten opnemen in boodschappenlijst', functionName: 'gerechtenOpnemenInBoodschappenlijst'},
        {name: 'Producten opnemen in boodschappenlijst', functionName: 'productenInBoodschappenlijstOpnemen'},
        {name: 'Boodschappen sorteren', functionName: 'plaatsPadBijProductInBoodschappenlijst'}
    ];
    spreadsheet.addMenu('Boodschappenlijst', menuItems);
}

function gerechtenOpnemenInBoodschappenlijst(){
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var shGerechtenSelectie = ss.getSheetByName('gerechten selectie');

    // geselecteerde gerechten
    var gerechtenSelectieRange = getTableRange(shGerechtenSelectie, 3);
    var geselecteerdeGerechten = gerechtenSelectieRange.getValues().filter(a => a[1] !== '').map(a => a[0]); //filter op kolom 'kiezen' en map naar kolom 'naam'

    // producten van de geselecteerde gerechten
    var shGerechten = ss.getSheetByName('gerechten');
    var gerechtenRange = getTableRange(shGerechten, 3);
    var products = gerechtenRange.getValues().reduce((p, c, i, a) => {

        if(geselecteerdeGerechten.indexOf(c[0]) > -1){
            p.push(c);
        }

        return p;
    }, []);


    createAndShowModal(products);
}


function createAndShowModal(productsValueArr) {

// Display a modal dialog box with custom HtmlService content.
    let htmlStart = '<html><head><base target="_top"></head> <body><form name="gerechtenForm">';

    let productHtml = productsValueArr.map(p => {
        return `<li>${p[1]} (${p[0]}) <input type="text" name="${p[0]}_${p[1]}" value="${p[2]}"></li>`
    });


    let htmlBody = `<h1>Producten voor gerechten</h1> <p>Wijzig eventueel de aantallen</p><ul>${productHtml}</ul>`;
    let fnGetFormData = `<script>function getFormData(){
    var formData = document.forms.gerechtenForm;
    google.script.run
    .withSuccessHandler(google.script.host.close)
    .verwerkGeselecteerdeGerechtenInBoodschappenlijst(formData)}
  </script>`;
    let htmlEnd = '<button onClick = "getFormData();">Opslaan</button></form> </body></html>'
    let html = htmlStart + htmlBody + fnGetFormData + htmlEnd;
    let htmlOutput = HtmlService
        .createHtmlOutput(html)
        .setWidth(550)
        .setHeight(300);
    SpreadsheetApp.getUi().showModalDialog(htmlOutput, 'Selecteer producten');

}

function verwerkGeselecteerdeGerechtenInBoodschappenlijst(formDataObject){

    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var shBoodschappenlijst = ss.getSheetByName('boodschappenlijst');
    var boodschappenlijstRange = getTableRange(shBoodschappenlijst, 4);
    var productenAanwezigInBoodschappenlijst = boodschappenlijstRange? boodschappenlijstRange.getValues().map(a => a[1]) : [];


    Object.keys(formDataObject).forEach(k => {

        let gerechtNaam = k.split('_')[0];
        let productNaam = k.split('_')[1];
        let productAantal = parseInt(formDataObject[k]);

        if(productenAanwezigInBoodschappenlijst.indexOf(productNaam) > -1){

            // product op boodschappenlijst bestaat -> product aantal ophogen met aantal afkomstig uit formulier
            for(let [i,v] of boodschappenlijstRange.getValues().entries()){


                if(v[1] == productNaam){

                    let nieuwAantal = v[0] == 0? productAantal + 1 : v[0] + productAantal;
                    shBoodschappenlijst.getRange(i+2,1).setValue(nieuwAantal);
                }

            };

        }else{

            // product op boodschappenlijst bestaat niet -> product toevoegen

            var lastRow = shBoodschappenlijst.getLastRow();
            shBoodschappenlijst.getRange(lastRow + 1, 1).setValue(productAantal);
            shBoodschappenlijst.getRange(lastRow + 1, 2).setValue(productNaam);

        }

    });

    plaatsPadBijProductInBoodschappenlijst()

}
